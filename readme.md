## Plotter for trading technical indicators 
Retroactively executes trading strategies on historical price data. (local sqlite db dump)
Plots technical indicator values, price data,  and strategy behavior.

####  Installation
> apt-get install python3  
pip3 install matplotlib  
pip3 install js2py  
pip3 install py_mini_racer
