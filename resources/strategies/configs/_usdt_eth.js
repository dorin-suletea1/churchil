/* ######################
Type            : Binance usdt_eth
Spiky market    : 17%% (Jan-July 2018)
Flat market     : 9% (Aug-Nov 2018)
#######################*/
var buyIfBelowBolByPercent = 1;
var sellIfAboveBolByPercent = 0.5;
var resolutionCandles = 14*60;


var emaResolutionA = 5*60;
var emaResolutionB = 30*60;
var macdCandleResolution = 80;

var macdMinimumDirectionToBuy = 0.2;
var macdAbortIncline = -0.1;

// logging
var directionMultiplier = 0.1;
var inclineMultiplier =   10;
//#######################
//#######################
//#######################
