/* ######################
Type            : Binance usdt_eth
Spiky market    : 36%% (Jan-July 2018)
Flat market     : 8% (Aug-Nov 2018)
#######################*/
var resolutionCandles = 24 * 60;
var macdConfirmationsRequired = 70;
var shortEmaResolution = 5 * 60;
var longEmaResolution = 20 * 60;
var buyMacdAngle = 0.0000017;
var sellEmergencyMacdAngle = -0.000001;
var sellEmergencyAtPriceDropPercent = 4;
var buyIfBelowBolByPercent = 0;
var sellIfAboveBolByPercent = 0;
//#######################
//#######################
//#######################