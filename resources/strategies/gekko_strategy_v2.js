var log = require('../core/log');
var strat = {};

/* #########################
 Online js compiler : http://esprima.org/demo/validate.html
 Buy : red   ; long
 Sell: green ; short
 console.log for plotter
 console.error for trader*/


var macdDistances=[]
var priceList = [];
var readyToTrade = false ;

/* ######################
Type            : Binance usdt_eth
Spiky market    : 36%% (Jan-July 2018)
Flat market     : 8% (Aug-Nov 2018)
#######################*/
var resolutionCandles = 24 * 60;
var macdConfirmationsRequired = 70;
var shortEmaResolution = 5 * 60;
var longEmaResolution = 20 * 60;
var buyMacdAngle = 0.0000017;
var sellEmergencyMacdAngle = -0.000001;
var sellEmergencyAtPriceDropPercent = 4;
var buyIfBelowBolByPercent = 0;
var sellIfAboveBolByPercent = 0;
//#######################
//#######################
//#######################
function addToPriceList(newPrice){
    if (priceList.length == resolutionCandles){
        priceList.splice(0,1);
    }
    priceList.push(newPrice);
}
function addToMacdDistances(shortEma,longEma){
    var diff = shortEma - longEma;
    if (macdDistances.length == macdConfirmationsRequired){
        macdDistances.splice(0,1);
    }
    macdDistances.push(diff);
}

function percentCalculation(a, b){
    if (b < 0){
        var c = (parseFloat(a)*parseFloat(-b))/100;
        return - parseFloat(c);
    }else{
        var c = (parseFloat(a)*parseFloat(b))/100;
        return parseFloat(c);
    }
}
function scientificToDecimal(num) {
    if(/\d+\.?\d*e[\+\-]*\d+/i.test(num)) {
        var zero = '0',
            parts = String(num).toLowerCase().split('e'), //split into coeff and exponent
            e = parts.pop(),//store the exponential part
            l = Math.abs(e), //get the number of zeros
            sign = e/l,
            coeff_array = parts[0].split('.');
        if(sign === -1) {
            coeff_array[0] = Math.abs(coeff_array[0]);
            num = '-'+zero + '.' + new Array(l).join(zero) + coeff_array.join('');
        }
        else {
            var dec = coeff_array[1];
            if(dec) l = l - dec.length;
            num = coeff_array.join('') + new Array(l+1).join(zero);
        }
    }
    return num;
};
function scientificArrayToDecimal(original){
    return original.map(function(each_element){
        return scientificToDecimal(each_element);
    });
}
//######################
//######################
//######################
shortEma = null;
longEma = null;
buyLine = null;
sellLine = null;
lastClosePrice = null;
currentCandleTime = null;
buyLog = "";
sellLog = "";
momentumIncline = null;
momentumDirection = null;
macdTrend = null;
previousPurchasePrice = null;
function simpleMovingAverage(){
    var total = priceList.reduce(function(a, b) { return a + b; }, 0);
    return total / priceList.length;
}
function standardDeviation(){
    var total = priceList.reduce(function(a, b) { return a + b; }, 0);
    var mean = total / priceList.length;
    var temp = 0;
    for (var j = 0 ; j < priceList.length; j++){
        temp = temp + ((priceList[j] - mean) * (priceList[j] - mean));
    }
    var variance = temp / (priceList.length - 1);
    return Math.sqrt(variance);
}
function bolinger(priceList){
    var ret = [];
    var sma =  simpleMovingAverage(priceList);
    var standardDev = standardDeviation(priceList);
    ret.push( sma + standardDev );
    ret.push( sma );
    ret.push( sma - standardDev );
    return ret;
}
function exponentialMovingAverage(resolution, previousEma){
    if (previousEma.length == 0){
        previousEma.push(simpleMovingAverage(priceList));
    }
    var total = priceList.reduce(function(a, b) { return a + b; }, 0);
    var newEma = (priceList[priceList.length-1] - previousEma[0]) * 2/(resolution+1) + previousEma[0];
    previousEma[0] = newEma;
    return newEma;
}

var previousShortEma = [];
function getShortEma(){
    return exponentialMovingAverage(shortEmaResolution, previousShortEma)
}
var previousLongEma = [];
function getLongEma(){
    return exponentialMovingAverage(longEmaResolution, previousLongEma)
}
function getMacdTrend(){
    var leftPart = macdDistances.slice(0,Math.ceil(macdDistances.length / 2));
    var rightPart = macdDistances.slice(Math.ceil(macdDistances.length / 2), macdDistances.length);

    var leftPartSum = leftPart.reduce(function(a, b) { return a + b; }, 0);
    var rightPartSum = rightPart.reduce(function(a, b) { return a + b; }, 0);
    var increase = rightPartSum - leftPartSum ;
    return increase;
}
function priceDroppedByPercent(percent){
    return lastClosePrice < previousPurchasePrice - percentCalculation(previousPurchasePrice, percent)
}
//######################
//######################
//######################
//######################
//######################
//######################
function resetLogs(){
    sellLog = "";
    buyLog = "";
}
function onCandle(candle){
    resetLogs()
    addToPriceList(candle.close);
    if (priceList.length < resolutionCandles){
        return;
    }
    readyToTrade = true;
    shortEma = getShortEma()
    longEma = getLongEma()
    addToMacdDistances(longEma, shortEma)

    currentCandleTime = candle.start;
    lastClosePrice = candle.close;

    var bol = bolinger(priceList);
    buyLine = bol[2] - percentCalculation(bol[2], buyIfBelowBolByPercent);
    sellLine = bol[0] + percentCalculation( bol[0], sellIfAboveBolByPercent);

    macdTrend = getMacdTrend()
}

function shouldBuy(){
    if (!readyToTrade){ return false }

    var shouldBuy = lastClosePrice < buyLine &&
                    macdTrend > buyMacdAngle;
    if (shouldBuy){
        previousPurchasePrice = lastClosePrice;
        buyLog = buyLog + " macd:{"+macdTrend+"}";
        return true;
    }
    return false;
}

function shouldSell(){
    if (!readyToTrade){ return false }

    if (priceDroppedByPercent(sellEmergencyAtPriceDropPercent)){
        sellLog = sellLog+" Abort percent loss";
        return true;
    }

    if (macdTrend < sellEmergencyMacdAngle){
        sellLog = sellLog+" Abort {macd:{"+macdTrend+"}}"
        return true;
    }
    if (lastClosePrice > sellLine ){
        return true;
    }


    return false;
}

function getIndicators(){
    if (!readyToTrade){
        return [null, null]
    }
    return [sellLine, buyLine];
}
function getPercentageIndicators(){
    if (!readyToTrade){
        return [null, null, null]
    }
    return [shortEma , longEma, macdTrend];
}
function getBuyLog(){return buyLog;}
function getSellLog(){return sellLog;}
strat.check = function() {
	if (!readyToTrade) {return;}
	if (shouldBuy()){this.advice('long');}
	if (shouldSell()){this.advice('short')}
}
strat.update = function(candle) {onCandle(candle);}
strat.init = function() {}
strat.log = function() {}
module.exports = strat;