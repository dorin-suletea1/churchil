from _datetime import datetime


class Timer:

    def __init__(self):
        self._previous = 0

    def tick(self, tag):
        now = datetime.now().timestamp()
        delta = now - self._previous
        self._previous = now
        print(tag + " : "+str(round(delta, 3)))

