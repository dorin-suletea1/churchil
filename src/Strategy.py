import os
from abc import abstractmethod

import js2py
from py_mini_racer import py_mini_racer

from src.Candle import Transaction


class Strategy:

    def __init__(self, javascript_engine, strategy_file_name):
        strategy_str = self._prepare_strategy(strategy_file_name)
        self._engine = javascript_engine
        self._engine.load_strategy_string(strategy_str)
        self.buy_signals = []
        self.sell_signals = []
        self._indicators = []
        self._percentage_indicators = []
        self._previous_buy_log = ""

    def _prepare_strategy(self, strategy_file_name):
        raw_strategy = self._load_strategy_file(strategy_file_name)
        sanitized_strategy = self.sanitize_strategy(raw_strategy)
        return sanitized_strategy

    def _load_strategy_file(self, strategy_file_name):
        this_path = os.path.dirname(__file__)
        parent_dir_path = os.path.abspath(os.path.join(this_path, os.pardir))
        absolute_file_path = parent_dir_path + "/resources/strategies/" + strategy_file_name + ".js"
        f = open(absolute_file_path, "r")
        contents = f.read()
        f.close()
        return contents

    def sanitize_strategy(self, raw_strategy):
        sanitized_strategy = ""
        prohibited_line_markers = ["require", "module.exports"]
        for line in raw_strategy.splitlines():
            line_ok = True
            for marker in prohibited_line_markers:
                if marker in line:
                    line_ok = False
            if line_ok:
                sanitized_strategy = sanitized_strategy + line + "\n"
        return sanitized_strategy

    def _on_candle(self, candle_do):
        return self._engine.on_candle(candle_do)

    def _do_buy(self):
        return self._engine.do_buy()

    def _do_sell(self):
        return self._engine.do_sell()

    def _get_indicator(self):
        return self._engine.get_indicator()

    def _get_percentage_indicator(self):
        return self._engine.get_percentage_indicator()

    def _get_indicator(self):
        return self._engine.get_indicator()

    @abstractmethod
    def save_signals(self, do_buy, do_sell, timestamp, price):
        raise Exception("Abstract method")

    @abstractmethod
    def get_transactions(self):
        raise Exception("Abstract method")

    # the price used by trader is the next tick
    def update(self, candle_do):
        self._on_candle(candle_do)
        buy = self._do_buy()
        sell = self._do_sell()
        timestamp = candle_do.timestamp
        self.save_signals(buy, sell, timestamp, candle_do.close_price)
        self._indicators.append(self._get_indicator())
        self._percentage_indicators.append(self._get_percentage_indicator())

    def get_signals(self):
        return [self.buy_signals, self.sell_signals]

    def get_percentage_indicators(self):
        ret = []
        for i in range(0, len(self._percentage_indicators[0])):
            current_indicator = []
            for j in range(0, len(self._percentage_indicators)):
                current_indicator.append(self._percentage_indicators[j][i])
            ret.append(current_indicator)
        return ret

    def get_indicators(self):
        ret = []
        for i in range(0, len(self._indicators[0])):
            current_indicator = []
            for j in range(0, len(self._indicators)):
                current_indicator.append(self._indicators[j][i])
            ret.append(current_indicator)
        return ret


class AllSignalStrategy(Strategy):
    def save_signals(self, do_buy, do_sell, timestamp, price):
        if do_buy:
            self.buy_signals.append(timestamp)
        if do_sell:
            self.sell_signals.append(timestamp)

    def get_transactions(self):
        return []


class NormalStrategy(Strategy):

    def __init__(self, javascript_engine, strategy_file_name, fee_provider):
        Strategy.__init__(self, javascript_engine, strategy_file_name)
        self._transactions = []
        self._trade_in_process = False
        self._bought_at_ts = None
        self._bought_at_price = None
        self._fee_provider = fee_provider

    def save_signals(self, do_buy, do_sell, timestamp, price):
        if self._trade_in_process and do_sell:
            self._trade_in_process = False
            transaction = Transaction(self._bought_at_ts, timestamp, self._bought_at_price, price, self._previous_buy_log, self._engine.get_sell_log(), self._fee_provider)
            self._transactions.append(transaction)
            self._bought_at_ts = None
            self._bought_at_price = None
            self._previous_buy_log = None
            self.sell_signals.append(timestamp)

        if not self._trade_in_process and do_buy:
            self._trade_in_process = True
            self._bought_at_ts = timestamp
            self._bought_at_price = price
            self.buy_signals.append(timestamp)
            self._previous_buy_log = self._engine.get_buy_log()

    @staticmethod
    def percentage(part, whole):
        return 100 * float(part) / float(whole)

    def get_transactions(self):
        return self._transactions


class Js2PyEngine:
    def __init__(self):
        self._context = js2py.EvalJs({'python_sum': sum})

    def load_strategy_string(self, sanitized_strategy_string):
        self._context.execute(sanitized_strategy_string)

    def on_candle(self, candle_do):
        candle = js2py.eval_js('d = {close:' + str(candle_do.close_price) + ', closeTime:' + str(candle_do.timestamp) + '}')
        return self._context.onCandle(candle)

    def do_buy(self):
        return self._context.shouldBuy()

    def do_sell(self):
        return self._context.shouldSell()

    def get_indicator(self):
        return self._context.getIndicators()

    def get_buy_log(self):
        return self._context.getBuyLog()

    def get_sell_log(self):
        return self._context.getSellLog()

    def get_percentage_indicator(self):
        return self._context.getPercentageIndicators()

    def get_indicator(self):
        return self._context.getIndicators()


class MiniRacerEngine:

    def __init__(self, is_gekko):
        self._context = py_mini_racer.MiniRacer()
        if is_gekko:
            self._close_price_tag = "close"
            self._close_time = "start"
        else:
            self._close_price_tag = "close"
            self._close_time = "closeTime"

    def load_strategy_string(self, sanitized_strategy_string):
        self._context.eval(sanitized_strategy_string)

    def on_candle(self, candle_do):
        candle = {self._close_price_tag: candle_do.close_price, self._close_time: str(candle_do.timestamp)}
        return self._context.call("onCandle", candle)

    def do_buy(self):
        return self._context.call("shouldBuy")

    def do_sell(self):
        return self._context.call("shouldSell")

    def get_indicator(self):
        return self._context.call("getIndicators")

    def get_buy_log(self):
        return self._context.call("getBuyLog")

    def get_sell_log(self):
        return self._context.call("getSellLog")

    def get_percentage_indicator(self):
        return self._context.call("getPercentageIndicators")
