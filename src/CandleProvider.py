import datetime


class CandleProvider:

    def __init__(self, database_importer):
        self._database_importer = database_importer

    def get_candles(self, pair, candle_resolution='m', from_ts=0, to_ts=datetime.datetime.now().timestamp()):
        candles = self._database_importer.get_candles_for_pair(pair, from_ts, to_ts)
        if candle_resolution is 'm':
            return candles
        if candle_resolution is 'h':
            return self._minute_to_hourly(candles)

    @staticmethod
    def _minute_to_hourly(minute_candles):
        minutes_in_hour = 60
        ret = []
        for i in range(0, len(minute_candles), minutes_in_hour):
            ret.append(minute_candles[i])
        return ret


