import datetime
import sqlite3


from src.Candle import Candle


class GekkoDatabaseImporter:

    def __init__(self, db_file_path):
        self._db_file_path = db_file_path
        print(self._db_file_path)

    def _run_get_query(self, query):
        conn = sqlite3.connect(self._db_file_path)
        cursor = conn.cursor()
        cursor.execute(query)
        rows = cursor.fetchall()
        return rows

    def get_table_names(self):
        return self._run_get_query("SELECT name FROM sqlite_master WHERE type='table'")

    def get_schema(self, table_name):
        return self._run_get_query("select sql from sqlite_master where type = 'table' and name = '" + table_name + "'")

    def get_candles_for_pair(self, pair, from_ts=0, to_ts=datetime.datetime.now().timestamp()):
        ret = []
        query = "SELECT * FROM " + pair + " where start  > " + str(from_ts) + " AND start < " + str(to_ts) + " ORDER BY start"
        rows = self._run_get_query(query)
        for row in rows:
            close = row[4]
            ts = row[1]
            ret.append(Candle(close, ts))
        return ret

    def _ts_to_sqlite_date(self, timestamp):
        return datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d')