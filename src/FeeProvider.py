from abc import abstractmethod


class FeeProvider:

    @abstractmethod
    def get_fee(self):
        raise Exception("Abstract method")

    @abstractmethod
    def get_slippage(self):
        raise Exception("Abstract method")


class BinanceFeeProvider(FeeProvider):

    def get_fee(self):
        return 0.1

    def get_slippage(self):
        return 0.05
