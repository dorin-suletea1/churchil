from src.CandleProvider import CandleProvider
from src.DatabaseImporter import GekkoDatabaseImporter
from src.FeeProvider import BinanceFeeProvider
from src.Plotter import Plotter
from src.Strategy import Js2PyEngine
from src.Strategy import MiniRacerEngine
from src.Strategy import NormalStrategy

IS_GEKKO = True
plotter = Plotter()
# standalone | fast
standalone_js_engine = Js2PyEngine()
v8_js_engine = MiniRacerEngine(IS_GEKKO)
# engine = standalone_js_engine
engine = v8_js_engine

strategy = NormalStrategy(engine, "gekko_strategy_v2", BinanceFeeProvider())
candle_resolution = 'm'

BTC_ADA = "candles_BTC_ADA"
USDT_ETH = "candles_USDT_ETH"

JANUARY_6 = 1515245113
JANUARY_20 = 1516453239
FEB_12 = 1518438627
MARCH_09 = 1520607507
MARCH_11 = 1520726401
APR_11 = 1523458724
MAY_11 = 1526045113
JUL_11 = 1531267201
JUL_21 = 1531353601

AUG_11 = 1536677924
SEP_11 = 1536677924
SEP_28 = 1538146724
OCT_11 = 1539269924
NOV_1 = 1541084324
NOV_10 = 1541860227
DEC_6 = 1544118114
#######
# pair
######

JULY_9 = 1531157153
JUNE_15 = 1534353953

#2018-07-09 14:41
#2018-12-06 17:41

pair = BTC_ADA
interval_start = AUG_11
interval_end = DEC_6


def percent(total, current):
    if current == 0:
        return 0
    return current / total * 100


def run():
    data_importer = GekkoDatabaseImporter("/home/binance_0.1.db")

    candle_provider = CandleProvider(data_importer)
    candles = candle_provider.get_candles(pair, candle_resolution, interval_start, interval_end)

    for i in range(0, len(candles) - 1):
        strategy.update(candles[i])
        if i % 500 == 0:
            print(str(percent(len(candles), i)) + "%")
    print("##############################")
    transactions = strategy.get_transactions()
    print("Count " + str(len(transactions)))
    print("Gains " + str(sum(transaction.get_gains() for transaction in transactions)))
    print("##############################")
    for transaction in transactions:
        print(str(transaction))
    action_signals = strategy.get_signals()
    indicators = strategy.get_indicators()
    percentage_indicators = strategy.get_percentage_indicators()
    print("##############################")
    plotter.plot_candles(candles[:-1], candle_resolution, action_signals, indicators, percentage_indicators)


run()
