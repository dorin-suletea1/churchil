import datetime


class Candle:

    def __init__(self, close_price, timestamp):
        self.close_price = close_price
        self.timestamp = timestamp

    def __str__(self):
        return str(self.close_price) + " " + str(self.timestamp) + "; "

    def __repr__(self):
        return self.__str__()


class Transaction:
    def __init__(self, start, end, buy_price, sell_price, buy_log, sell_log, fee_provider):
        self._start = start
        self._end = end
        self._buy_price = buy_price
        self._sell_price = sell_price
        diff = sell_price - buy_price
        self._gain_pc = (100 * float(diff) / float(buy_price)) - fee_provider.get_fee() * 2
        self._buy_log = buy_log
        self._sell_log = sell_log

    def get_gains(self):
        return self._gain_pc

    def __str__(self):
        start_readable = datetime.datetime.fromtimestamp(self._start).strftime('%Y-%m-%d %H:%M')
        end_readable = datetime.datetime.fromtimestamp(self._end).strftime('%Y-%m-%d %H:%M')
        return "{" + '"buy":' + str(start_readable) + ' , "sell":' + str(end_readable) + \
               ', "buyPrice":' + str(format(self._buy_price, '.8f')) + \
               ', "sellPrice":' + str(format(self._sell_price, '.8f')) + \
               ', "gain":' + str(self._gain_pc) + \
               ', "buyLog":' + str(self._buy_log) + \
               ', "sellLog":' + str(self._sell_log) + \
               " }"


def __repr__(self):
    return self.__str__()
