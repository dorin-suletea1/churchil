import matplotlib.pyplot as plt
import matplotlib.ticker as ticker


class Plotter:
    _millis_in_minute = 60000
    _millis_in_hour = 3600000

    def plot_candles(self, candle_list, candle_type='m', action_signals=[[], []], indicators=[], percentage_indicators=[]):
        close_prices = [candle.close_price for candle in candle_list]
        close_time = [candle.timestamp for candle in candle_list]

        ax = plt.subplot(211)
        ax.xaxis.set_major_locator(ticker.AutoLocator())

        plt.plot(close_time, close_prices)
        # buy
        buy_signals = action_signals[0]
        for xc in buy_signals:
            plt.axvline(x=xc, color='r', linestyle='--')
        # sell
        sell_signals = action_signals[1]
        for xc in sell_signals:
            plt.axvline(x=xc, color='g', linestyle='--')
        # indicators
        for indicator in indicators:
            plt.plot(close_time, indicator)
        # percentage indicators
        plt.subplot(212, sharex=ax)
        for percentage_indicator in percentage_indicators:
            plt.plot(close_time, percentage_indicator)
        plt.show()
